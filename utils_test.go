package limelightstorage

import "testing"

func Test_getDirNameFromFullPath(t *testing.T) {
	type args struct {
		fullPath string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		want1   string
		wantErr bool
	}{
		{
			"T1",
			args{"/testing/testing.mp4"},
			"/testing/",
			"testing.mp4",
			false,
		},
		{
			"T2",
			args{"/testing/test_2/testing.mp4"},
			"/testing/test_2/",
			"testing.mp4",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := getDirNameFromFullPath(tt.args.fullPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("getDirNameFromFullPath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getDirNameFromFullPath() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("getDirNameFromFullPath() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
