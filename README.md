# Limelight Network Storage Go Client
Limelight Orchestrate Cloud Storage Api Client: under development

## Features

* Login and UploadFileNonMultipart finished
* Hysterix and retries(3) already implement for Login API
* Uses rpc and http clients under the hood for login and file upload respectively

## Usage
    LLRpcOriginURL := "https://{AccountName}.upload.llnw.net"
    LLRpcOriginAccount := "AccountName"
    LLUsername := ""
    LLPassword := ""
    ctx := context.Background()
	serviceClient, err := NewClient(LLRpcOriginURL, LLRpcOriginAccount)
	if err != nil {
		panic(err)
	}
	_, err = serviceClient.Login(ctx, LLUsername, LLPassword, false)
	if err != nil {
		panic(err)
	}
	localFilepath := "/tmp/x.mp4"
	limelightFullPath := "/originstorage/full/path/xy.mp4"
	log.Println("uploading file")
	err = serviceClient.UploadFileNonMultipart(ctx, localFilepath, limelightFullPath)
	if err != nil {
		t.Error(err)
	}
	log.Println("uploaded file")
