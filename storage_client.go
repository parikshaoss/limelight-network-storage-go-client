package limelightstorage

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	jsonrpc "github.com/ethereum/go-ethereum/rpc"
	"gopkg.in/eapache/go-resiliency.v1/retrier"
)

type storageServiceClient struct {
	lLRpcOriginURL     string
	lLRpcOriginAccount string
	llAuthToken        string
	rpcClient          *jsonrpc.Client
	httpClient         *http.Client
}

// StorageServiceClient contains wrapper for functions
type StorageServiceClient interface {
	Login(context.Context, string, string, bool) (string, error)
	UploadFileNonMultipart(context.Context, string, string) error
}

func init() {
	hystrix.ConfigureCommand("login", hystrix.CommandConfig{
		Timeout:               3000,
		MaxConcurrentRequests: 100,
		ErrorPercentThreshold: 25,
	})

}

// NewClient create a new StorageServiceClient
func NewClient(LLRpcOriginURL, LLRpcOriginAccount string) (StorageServiceClient, error) {
	ctx := context.Background()

	serverURL := fmt.Sprintf(LLRpcOriginURL+"%s", "/jsonrpc2")

	r := retrier.New(retrier.ExponentialBackoff(3, 200*time.Millisecond), nil)
	output := make(chan *jsonrpc.Client, 1)
	err := r.Run(func() error {
		rpcClient, err := jsonrpc.DialContext(ctx, serverURL)
		if err != nil {
			return err
		}
		// talk to other services
		output <- rpcClient
		return nil
	})
	if err != nil {
		return nil, err
	}

	httpClient := &http.Client{}
	return &storageServiceClient{
		lLRpcOriginURL:     LLRpcOriginURL,
		lLRpcOriginAccount: LLRpcOriginAccount,
		rpcClient:          <-output,
		httpClient:         httpClient,
	}, nil
	// return NewClientWithOptions(rpcClient)
}

// func NewClientWithOptions() StorageServiceClient {

// }

// ErrUnexpectedResponse is returned when the response from limelight is not in conjunction with what is provided in docs
var ErrUnexpectedResponse = errors.New("Unexpected response from limelightstorage cloud")

// Login call uses login func with retries and circuitbreaker
func (client *storageServiceClient) Login(ctx context.Context, username, password string, additionalDetail bool) (string, error) {
	output := make(chan string, 1)
	errors := hystrix.Go("login", func() error {
		r := retrier.New(retrier.ConstantBackoff(3, 200*time.Millisecond), nil)

		err := r.Run(func() error {
			// log.Println("trying")
			token, err := client.login(ctx, username, password, additionalDetail)
			if err != nil {
				return err
			}
			client.llAuthToken = token
			// talk to other services
			output <- token
			return nil
		})

		if err != nil {
			// handle the case where the work failed three times
			return err
		}
		return nil
	}, nil)

	select {
	case out := <-output:
		// success
		return out, nil
	case err := <-errors:
		log.Println(err)
		// failure
		return "", err
	}
}

// login call uses login of limelight storage rpc api
func (client *storageServiceClient) login(ctx context.Context, username, password string, additionalDetail bool) (string, error) {
	params := []interface{}{
		username,
		password,
		additionalDetail,
	}
	result := []interface{}{}
	token := ""
	ok := false
	err := client.rpcClient.CallContext(ctx, &result, "login", params...)
	if err != nil {
		return "", err
	}
	// log.Println("Result", result)
	token, ok = result[0].(string)
	if !ok {
		return "", ErrUnexpectedResponse
	}
	// log.Println("Token", token)
	return token, nil
}

// UploadFileNonMultipart allows to upload files less than 10GB, for more than 10GB use multi-part upload
func (client *storageServiceClient) UploadFileNonMultipart(ctx context.Context, localFilepath, limelightFullPath string) error {
	return client.uploadFileNonMultipart(ctx, localFilepath, limelightFullPath)
}

func (client *storageServiceClient) uploadFileNonMultipart(ctx context.Context, localFilepath string, limelightFullPath string) error {
	serverURL := client.lLRpcOriginURL + "/post/raw"

	file, err := os.Open(localFilepath)
	if err != nil {
		return err
	}
	defer file.Close()

	dir, filename, err := getDirNameFromFullPath(limelightFullPath)
	if err != nil {
		return err
	}
	// bufio.NewReader(file)
	req, err := http.NewRequest("POST", serverURL, file)
	if err != nil {
		return err
	}
	req.Header.Set("X-Agile-Authorization", client.llAuthToken)
	req.Header.Set("X-Agile-Directory", dir)
	req.Header.Set("X-Agile-Recursive", "1")
	req.Header.Set("X-Agile-Basename", filename)
	res, err := client.httpClient.Do(req)
	if err != nil {
		return err
	}
	agileStatus := res.Header.Get("x-agile-status")
	log.Println(client.llAuthToken)
	log.Println(agileStatus)
	if agileStatus == "0" {
		return nil
	}
	return ErrUnexpectedResponse
}
