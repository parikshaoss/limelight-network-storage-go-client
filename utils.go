package limelightstorage

import (
	"context"
	"strings"
	"time"
)

func sleepTillTimeOrSignal(ctx context.Context, t time.Duration) {
	timer := time.NewTimer(t)
	select {
	case <-ctx.Done():
		return
	case <-timer.C:
		return
	}
}

func getDirNameFromFullPath(fullPath string) (string, string, error) {
	parts := strings.Split(fullPath, "/")
	filename := ""
	dirPath := strings.Join(parts[0:len(parts)-1], "/") + "/"
	if len(parts) == 1 {
		filename = ""
		return dirPath, filename, nil
	}

	filename = parts[len(parts)-1]
	return dirPath, filename, nil
}
